Name: gitkraken
Version: 4.0.4
Release:        2%{?dist}
Summary: The legendary Git GUI client for Windows, Mac and Linux

Group: Development
License: MIT
URL: https://www.gitkraken.com/
Source0: https://release.gitkraken.com/linux/gitkraken-amd64.tar.gz
Source1: gitkraken.desktop
Source2: gitkraken.svg

BuildRequires: curl,libgnome-keyring-devel,libXScrnSaver
Requires: libgnome-keyring-devel,libXScrnSaver
AutoReq: no

%description
A full-featured Git GUI client, made with love


%prep
%autosetup -n gitkraken


%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}/%{name}
mkdir -p %{buildroot}%{_datadir}/applications/
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/

cp -vR %{_builddir}/%{name}/* %{buildroot}%{_datadir}/%{name}

ln -s /usr/share/gitkraken/gitkraken %{buildroot}%{_bindir}/
install -Dm644 %{SOURCE1} %{buildroot}%{_datadir}/applications/
install -Dm644 %{SOURCE2} %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/

%post
ln -sf /lib64/libcurl.so.4 /lib64/libcurl-gnutls.so.4 

%check


%files
%license LICENSE
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
/usr/share/gitkraken/


%changelog

